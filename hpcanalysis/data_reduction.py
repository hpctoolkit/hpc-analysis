# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

from abc import ABC, abstractmethod
from typing import List, Tuple


class DataReductionFilter(ABC):

    @abstractmethod
    def validate_node(
        self, type: str, name: str, time_percentage: int | None
    ) -> Tuple[bool, bool]:
        pass


class MPIReduction(DataReductionFilter):

    def validate_node(
        self, type: str, name: str, time_percentage: int | None
    ) -> Tuple[bool, bool]:
        include_node = True
        include_subtree = (
            not (name.startswith("MPI_") or name.startswith("PMPI_"))
            if name is not None
            else True
        )
        return (include_node, include_subtree)


class FunctionReduction(DataReductionFilter):

    def validate_node(
        self, type: str, name: str, time_percentage: int | None
    ) -> Tuple[bool, bool]:
        include_node = type == "function"
        include_subtree = True
        return (include_node, include_subtree)


class TimePercentageReduction(DataReductionFilter):

    def __init__(self, percentage_threshold: int) -> None:
        super().__init__()
        self._percentage_threshold = percentage_threshold

    def validate_node(
        self,
        type: str,
        name: str,
        time_percentage: int | None,  # TODO: check this None part
    ) -> Tuple[bool, bool]:
        if time_percentage is None:  # TODO: check this
            return True, True
        include_node = time_percentage >= self._percentage_threshold
        include_subtree = time_percentage >= self._percentage_threshold
        return (include_node, include_subtree)


class DataReduction:

    def __init__(self) -> None:
        self._reductions: List[DataReductionFilter] = []

    def add_reduction(self, reduction: DataReductionFilter) -> None:
        self._reductions.append(reduction)

    def apply_reductions(
        self, type: str, name: str, time_percentage: int | None
    ) -> Tuple[bool, bool]:
        include_node = True
        include_subtree = True
        for reduction in self._reductions:
            temp = reduction.validate_node(type, name, time_percentage)
            include_node = include_node and temp[0]
            include_subtree = include_subtree and temp[1]

        # TODO: check this!!

        return include_node, include_subtree
