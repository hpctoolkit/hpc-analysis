# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

from abc import ABC, abstractmethod
from typing import List, Tuple

import pandas as pd


class QueryAPI(ABC):
    @abstractmethod
    def query_cct(self, cct_exp: str | List[str]) -> pd.DataFrame:
        pass

    @abstractmethod
    def query_metric_descriptions(self, metrics_exp: str | List[str]) -> pd.DataFrame:
        pass

    @abstractmethod
    def query_profile_descriptions(self, profiles_exp: str | List[str]) -> pd.DataFrame:
        pass

    @abstractmethod
    def query_profile_slices(
        self,
        profiles_exp: str | List[str],
        cct_exp: str | List[str],
        metrics_exp: str | List[str],
    ) -> pd.DataFrame:
        pass

    @abstractmethod
    def query_trace_slices(
        self, profiles_exp: str | List[str], time_frame: Tuple[int, int] = None
    ) -> pd.DataFrame:
        pass
