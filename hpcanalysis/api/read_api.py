# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

from abc import ABC, abstractmethod
from typing import Dict, List, Tuple

from hpcanalysis.data_reduction import DataReduction, TimePercentageReduction


class ReadAPI(ABC):
    def __init__(
        self,
        dir_path: str,
        data_reduction: DataReduction = None,
    ) -> None:
        self._dir_path = dir_path
        self._data_reduction = data_reduction

    def has_data_reduction(self) -> bool:
        return (
            self._data_reduction is not None
            and len(self._data_reduction._reductions) > 0
        )

    def has_time_data_reduction(self) -> bool:
        if self._data_reduction is None:
            return False

        for reduction in self._data_reduction._reductions:
            if type(reduction) == TimePercentageReduction:
                return True

        return False

    @abstractmethod
    def read_cct(
        self,
    ) -> Tuple[
        List[Dict[str, str | int]],
        List[Dict[str, str | int]],
        List[Dict[str, str]],
        List[Dict[str, str]],
    ]:
        pass

    @abstractmethod
    def read_metric_descriptions(self) -> List[Dict[str, str | int]]:
        pass

    @abstractmethod
    def read_profile_descriptions(self) -> List[Dict[str, str | int]]:
        pass

    @abstractmethod
    def read_profile_slices(
        self,
        profile_indices: Dict[int, List[Tuple[int, int]]],
    ) -> List[Dict[str, int | float]]:
        pass

    @abstractmethod
    def read_trace_slices(
        self, profile_indices: Dict[int, List[Tuple[int, int]]]
    ) -> List[Dict[str, int]]:
        pass
