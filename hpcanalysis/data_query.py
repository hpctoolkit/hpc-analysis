# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

import re
from typing import List, Tuple

import pandas as pd

from hpcanalysis.api.query_api import QueryAPI
from hpcanalysis.api.read_api import ReadAPI
from hpcanalysis.cct import cct_is_child
from hpcanalysis.metrics import GPU_METRICS, TIME_METRICS
from hpcanalysis.queries import CCT_QUERY, METRICS_QUERY, PROFILES_QUERY

ENTRY_POINT_MAPPING = {0: "unknown entry", 1: "main thread", 2: "application thread"}

from hpcanalysis.tables_format import (
    format_cct_table,
    format_functions_table,
    format_metric_descriptions_table,
    format_profile_descriptions_table,
    format_source_files_table,
    format_load_modules_table,
)


def validate_exp(exp: List[str], pattern: str) -> None:
    if not len(exp):
        raise ValueError("ERROR: Wrong format of query expression!!")
    for item in exp:
        if item != "*" and not re.fullmatch(pattern, item):
            raise ValueError("ERROR: Wrong format of query expression!!")


def profiles_exp_to_query(exp: str) -> str:
    exp_array = exp.split(".")
    query_values = {}

    for item in exp_array:
        if item.endswith(")"):
            temp_array = item[:-1].split("(")
            item = temp_array[0]
            values_array = temp_array[1].split(",")
            values = []
            for value in values_array:
                if "-" in value:
                    start = int(value.split("-")[0])
                    end = value.split("-")[1]
                    if ":" in end:
                        step = int(end.split(":")[1])
                        end = int(end.split(":")[0])
                    else:
                        step = 1
                        end = int(end)
                    for i in range(start, end + 1, step):
                        values.append(i)
                else:
                    values.append(int(value))

            values = list(set(values))
            values.sort()
            query_values[item] = values

        else:
            query_values[item] = item

    query = ""
    for item in query_values:
        if query:
            query += " and "

        query += f"{item} {'==' if item == query_values[item] else 'in'} {query_values[item]}"

    return query


class DataQuery(QueryAPI):
    def __init__(self, read_api: ReadAPI) -> None:
        self._read_api = read_api

        self._cct = pd.DataFrame()
        self._profile_descriptions = pd.DataFrame()
        self._metric_descriptions = pd.DataFrame()
        self._profile_slices = pd.DataFrame()
        self._trace_slices = pd.DataFrame()

        self._functions = pd.DataFrame()
        self._source_files = pd.DataFrame()
        self._load_modules = pd.DataFrame()

    def query_cct(self, cct_exp: str | List[str]) -> pd.DataFrame:
        if not len(self._cct):
            cct, functions, source_files, load_modules = self._read_api.read_cct()
            self._cct = format_cct_table(pd.DataFrame(cct))

            self._functions = format_functions_table(pd.DataFrame(functions))
            self._source_files = format_source_files_table(pd.DataFrame(source_files))
            self._load_modules = format_load_modules_table(pd.DataFrame(load_modules))

        cct_exp = cct_exp if type(cct_exp) == list else [cct_exp]
        validate_exp(cct_exp, CCT_QUERY)

        if "*" in cct_exp:
            return self._cct.copy()

        final_result = []

        for c_exp in cct_exp:
            valid_expression = True

            exp_array = c_exp.split(").")
            cct_ids = []

            for item in exp_array[::-1]:
                ids = []
                if "(" in item:
                    node_type = item.split("(")[0].strip().lower()
                    identifier = item.split("(")[1].strip()
                    if identifier.endswith(")"):
                        identifier = identifier[:-1]
                else:
                    node_type = item
                    identifier = None

                temp = self._cct[self._cct["type"] == node_type]

                if identifier is not None:
                    if node_type == "entry":
                        temp = temp[
                            temp["name"].apply(
                                lambda x: ENTRY_POINT_MAPPING[x] == identifier
                            )
                        ]  # TODO: check this
                    elif node_type == "function":
                        temp2 = self._functions[
                            self._functions["name"].str.fullmatch(
                                identifier.replace("*", ".+")
                            )
                        ].index.tolist()
                        temp = temp[temp["name"].isin(temp2)]
                    elif node_type == "instruction":
                        module_path = identifier.split(":")[0].strip()
                        offset = int(identifier.split(":")[1].strip())
                        temp2 = self._load_modules[
                            self._load_modules["module_path"].str.fullmatch(
                                module_path.replace("*", ".+")
                            )
                        ].index.tolist()
                        temp = temp[
                            temp["module_path"].isin(temp2) & temp["offset"] == offset
                        ]
                    else:
                        file_path = identifier.split(":")[0].strip()
                        line = int(identifier.split(":")[1].strip())
                        temp2 = self._source_files[
                            self._source_files["file_path"].str.fullmatch(
                                file_path.replace("*", ".+")
                            )
                        ].index.tolist()
                        temp = temp[
                            temp["file_path"].isin(temp2) & temp["line"] == line
                        ]

                ids = temp.index.tolist()

                if not ids:
                    valid_expression = False
                    break

                cct_ids.append(ids)

            if not valid_expression:
                continue

            if len(cct_ids) == 1:
                final_result.extend(cct_ids[0])
                continue

            stack_of_ids = [cct_ids[0][:]]
            stack_of_paths = []
            result = []

            while True:

                if len(stack_of_ids) >= len(cct_ids):
                    result.append(stack_of_paths.pop())
                    stack_of_ids.pop()
                    while len(stack_of_ids) > 1:
                        stack_of_ids.pop()

                if not stack_of_ids[0]:
                    break

                up_level = cct_ids[len(stack_of_ids)]
                leaf_node = False
                if len(stack_of_ids) == 1:
                    leaf_node = True
                next_id = stack_of_ids[-1].pop()

                if leaf_node:
                    stack_of_paths.append(next_id)

                filtered_list = list(
                    filter(lambda x: cct_is_child(self._cct, next_id, x), up_level)
                )

                if filtered_list:
                    stack_of_ids.append(filtered_list)
                else:
                    stack_of_paths.pop()

            final_result.extend(result)

        return self._cct.loc[final_result]

    def query_profile_descriptions(self, profiles_exp: str | List[str]) -> pd.DataFrame:

        if not len(self._profile_descriptions):
            self._profile_descriptions = format_profile_descriptions_table(
                pd.DataFrame(self._read_api.read_profile_descriptions())
            )

        profiles_exp = profiles_exp if type(profiles_exp) == list else [profiles_exp]
        validate_exp(profiles_exp, PROFILES_QUERY)

        if "*" in profiles_exp:
            return self._profile_descriptions.copy()

        profiles_indices = []
        for item in profiles_exp:
            profiles_indices.extend(
                self._profile_descriptions[
                    self._profile_descriptions.index == 0
                ].index.unique()
                if item == "summary"
                else self._profile_descriptions.query(
                    profiles_exp_to_query(item)
                ).index.unique()
            )

        return self._profile_descriptions.loc[profiles_indices]

    def query_metric_descriptions(self, metrics_exp: str | List[str]) -> pd.DataFrame:

        if not len(self._metric_descriptions):
            self._metric_descriptions = format_metric_descriptions_table(
                pd.DataFrame(self._read_api.read_metric_descriptions())
            )

        metrics_exp = metrics_exp if type(metrics_exp) == list else [metrics_exp]
        validate_exp(metrics_exp, METRICS_QUERY)

        if "*" in metrics_exp:
            return self._metric_descriptions.copy()

        metric_ids = []

        for item in metrics_exp:
            name = item.split("(")[0].strip() if "(" in item else item
            if ":" in name:
                aggregation = name.split(":")[1]
                name = name.split(":")[0]
            else:
                aggregation = None
            names = (
                TIME_METRICS
                if name == "time" or name in TIME_METRICS
                else list(GPU_METRICS.keys()) if name == "gpu" else [name]
            )
            scopes = item.split("(")[1][:-1].split(",") if "(" in item else []

            metric_ids.extend(
                self._metric_descriptions[
                    self._metric_descriptions["name"].isin(names)
                    & (
                        self._metric_descriptions["scope"].isin(scopes)
                        if len(scopes)
                        else True
                    )
                    & (
                        self._metric_descriptions["aggregation"] == aggregation
                        if aggregation is not None
                        else self._metric_descriptions["aggregation"].isna()
                    )
                ].index.tolist()
            )

        return self._metric_descriptions.loc[metric_ids]

    def query_profile_slices(
        self,
        profiles_exp: str | List[str],
        cct_exp: str | List[str] | List[int],  # TODO: check this
        metrics_exp: str | List[str],
    ) -> pd.DataFrame:
        profile_indices = self.query_profile_descriptions(profiles_exp).index.unique()
        add_application = False

        cct_exp = cct_exp if type(cct_exp) == list else [cct_exp]
        if type(cct_exp[0]) == int:  # TODO: check this
            cct_indices = cct_exp  # TODO: check this
        else:
            if "application" in cct_exp:
                cct_exp.remove("application")
                add_application = True
            cct_indices = (
                self.query_cct(cct_exp).index.unique().tolist() if len(cct_exp) else []
            )

        if add_application:
            cct_indices.append(0)

        metric_indices = (
            self.query_metric_descriptions(metrics_exp)["id"].unique().tolist()
        )

        read_profile_indices = (
            profile_indices.difference(self._profile_slices.index.get_level_values(0))
            .unique()
            .tolist()
        )
        profile_indices = profile_indices.tolist()
        read_cct_indices = (
            self._cct.index.unique().tolist()
            if self._read_api.has_data_reduction()
            else []
        )
        if add_application and len(read_cct_indices):
            read_cct_indices.append(0)
        read_metric_indices = []  # TODO: check this

        read_cct_indices.sort()
        read_metric_indices.sort()

        table = pd.DataFrame(
            self._read_api.read_profile_slices(
                read_profile_indices, read_cct_indices, read_metric_indices
            )
        )

        if len(table):
            table = table.set_index(
                [
                    "profile_id",
                    "cct_id",
                    "metric_id",
                ]
            )

        self._profile_slices = pd.concat([self._profile_slices, table])

        return self._profile_slices[
            self._profile_slices.index.get_level_values(0).isin(profile_indices)
            & self._profile_slices.index.get_level_values(1).isin(cct_indices)
            & self._profile_slices.index.get_level_values(2).isin(metric_indices)
        ]

    def query_trace_slices(
        self, profiles_exp: str | List[str], time_frame: Tuple[int, int] = None
    ) -> pd.DataFrame:

        profiles_index = self.query_profile_descriptions(profiles_exp).index
        profiles_indices = profiles_index.unique().tolist()

        def group_f(x: pd.Series) -> pd.Series:
            return pd.Series(
                {
                    "start_timestamp": x.iloc[0]["start_timestamp"],
                    "end_timestamp": x.iloc[-1]["end_timestamp"],
                }
            )

        merge_dict = {}
        # TODO: merge previous arrays

        if len(self._trace_slices):
            temp = (
                (
                    self._trace_slices.query(
                        f"profile_id in {profiles_indices} and start_timestamp >= {time_frame[0]} and end_timestamp <= {time_frame[1]}"
                    )
                    if time_frame
                    else self._trace_slices.query(f"profile_id in {profiles_indices}")
                )
                .groupby("profile_id")
                .apply(group_f)
            )

            read_dict = {
                prof_index: [time_frame] if time_frame else []
                for prof_index in profiles_index.difference(
                    temp.index.unique().tolist()
                )
                .unique()
                .tolist()
            }

            if time_frame:

                for id, row in temp.iterrows():
                    time_frames = []

                    if (
                        not len(
                            self._trace_slices.query(
                                f"end_timestamp == {row['start_timestamp']}"
                            )
                        )
                        and time_frame[0] != row["start_timestamp"]
                    ):
                        time_frames.append((time_frame[0], row["start_timestamp"]))

                    if (
                        not len(
                            self._trace_slices.query(
                                f"start_timestamp == {row['end_timestamp']}"
                            )
                        )
                        and row["end_timestamp"] != time_frame[1]
                    ):
                        time_frames.append((row["end_timestamp"], time_frame[1]))

                    if len(time_frames):
                        read_dict[id] = time_frames

        else:
            read_dict = {
                prof_index: [time_frame] if time_frame else []
                for prof_index in profiles_indices
            }

        self._trace_slices = pd.concat(
            [
                self._trace_slices,
                pd.DataFrame(self._read_api.read_trace_slices(read_dict)),
            ]
        ).sort_values("start_timestamp")

        return (
            self._trace_slices.query(
                f"profile_id in {profiles_indices} and start_timestamp >= {time_frame[0]} and end_timestamp <= {time_frame[1]}"
            )
            if time_frame
            else self._trace_slices.query(f"profile_id in {profiles_indices}")
        )
