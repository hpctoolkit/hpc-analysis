# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

import pandas as pd


def reconstruct_subcct(
    cct: pd.DataFrame,
    subcct: pd.DataFrame,
) -> pd.DataFrame:

    temp_dict = {}

    for id, node in subcct.iterrows():

        temp_dict[id] = []

        while not pd.isna(node["parent"]):
            parent_id = node["parent"]

            if parent_id not in temp_dict:
                temp_dict[parent_id] = [id]
            elif id not in temp_dict[parent_id]:
                temp_dict[parent_id].append(id)

            id = parent_id
            node = cct.loc[parent_id]

    return cct.loc[temp_dict.keys()]


def cct_is_child(cct: pd.DataFrame, child_id: int, parent_id: int) -> bool:
    child_node = cct.loc[child_id]
    parent_depth = cct.loc[parent_id]["depth"]

    while child_node["depth"] > parent_depth:
        if child_node["parent"] == parent_id:
            return True
        child_node = cct.loc[child_node["parent"]]
    return False
