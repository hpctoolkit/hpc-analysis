# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

METRICS_QUERY = "(time|cputime|realtime|cycles|gpu|gpuop|gker|gmem|gmset|gxcopy|gicopy|gsync)(:(sum|min|max))?( \((i|e|p|c)(,(i|e|p|c))*\))?"
CCT_QUERY = "(entry|function|loop|line|instruction)(\(.+\))?(\.(entry|function|loop|line|instruction)(\(.+\))?)*"
PROFILES_QUERY = "summary|(node|rank|thread|gpudevice|gpucontext|gpustream|core)(\([0-9]+(-[0-9]+(:[0-9]+)?)?(,[0-9]+(-[0-9]+)?)*\))?(.(node|rank|thread|gpudevice|gpucontext|gpustream|core)(\([0-9]+(-[0-9]+(:[0-9]+)?)?(,[0-9]+(-[0-9]+)?)*\))?)*"
