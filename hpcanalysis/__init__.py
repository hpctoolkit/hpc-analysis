# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

from hpcanalysis.data_reduction import DataReduction
from hpcanalysis.data_analysis import DataAnalysis


def open_db(
    dir_path: str,
    connect_to_server: bool = False,
    data_reduction: DataReduction = None,
) -> DataAnalysis:
    if connect_to_server:
        raise ValueError("Fetching data from remote server not implemented yet!")

    from hpcanalysis.data_query import DataQuery
    from hpcanalysis.data_read import DataRead

    data_read = DataRead(dir_path, data_reduction=data_reduction)
    data_query = DataQuery(data_read)
    data_analysis = DataAnalysis(data_query)

    if data_read.has_time_data_reduction():
        try:
            time_metric_id = data_query.query_metric_descriptions("time:sum (i)")[
                "id"
            ].iloc[0]
            data_read._time_metric_id = time_metric_id  # TODO: check this
        except:
            pass

    return data_analysis
